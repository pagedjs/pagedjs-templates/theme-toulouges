class forensic extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    beforeParsed(content) {

        const sections = content.querySelectorAll('*');
        var classes = [];
        sections.forEach(element => {
            if(element.classList.value != '') {
            classes.push(element.classList.value);
            }
        });
        let uniqueClassStack = new Set(classes);
        console.log(uniqueClassStack)
    }
}
Paged.registerHandlers(forensic);