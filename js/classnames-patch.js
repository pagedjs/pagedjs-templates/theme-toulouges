class patchClasses extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    // renaming .section to .lebel-three
    content.querySelectorAll("section.section").forEach((section) => {
      section.className = "level-three"
    });
     // renaming .section to .lebel-three
     content.querySelectorAll("ol.chapter-notes").forEach((list) => {
      list.className = "level-two-notes"
    });
  }
}
Paged.registerHandlers(patchClasses);
