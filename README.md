---
author: Benoît Launay
title: Toulouges
project: paged.js sprint
book format: letter
book orientation: portrait
---

# Theme name: Toulouges

## Description

First experiment with a modular approach based on two references:

- one vertical dimension equals to one twelfth of the page height
- one horizontal dimension equals to one twelfth of the page width

These two dimensions are modified through basic mathematical operations in order to obtain values such as the line height, font size, spacing elements and so on…

## Typefaces

### Crimson Pro

Font fom Jacques Le Bailly used for the main text in the followings styles and widths:

- Regular
- Italic
- Extra Light
- Extra Light Italic
- Black
- Black Italic

repository on GitHub: [/Fonthausen/CrimsonPro](https://github.com/Fonthausen/CrimsonPro)

### Oswald

Font from Vernon Adams used for some titles in the following style and width:

- Oswald Bold

repository on GitHub: [/vernnobile/OswaldFont](https://github.com/vernnobile/OswaldFont)

## References

Toulouges:
    French town in the south of metropolitan France, near Perpignan, marginally known for the *Pau i Treva de Deu*[^pit] and its red onions[^tof].

Colors:
The colors used are based upon the town’s coat of arms[^her] : *or*, *gules* and *vert*.


[^pit]: [some information on wikipedia](https://en.wikipedia.org/wiki/Peace_and_Truce_of_God)

[^tof]: see *Know your (French) onions* on [tasteoffrancemag.com](https://tasteoffrancemag.com/trending/know-your-french-onions/)

[^her]: for information about colors’ names please see the following [article on wikipedia](https://en.wikipedia.org/wiki/Tincture_(heraldry))